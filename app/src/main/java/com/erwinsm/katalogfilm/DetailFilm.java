package com.erwinsm.katalogfilm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DetailFilm extends AppCompatActivity {

    Movie movie;
    Context context;
    private ImageView movieImageDetail;
    private TextView movieName, movieDescription, movieDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_film);
        Bundle bundle = getIntent().getBundleExtra("movie");
        movie = bundle.getParcelable("movie_detail");
        movieImageDetail = (ImageView)findViewById(R.id.movieImageDetail);
        String url =  new StringBuilder(movie.getGambar()).deleteCharAt(0).toString();
        Picasso.with(context).load("http://image.tmdb.org/t/p/w342/"+url).into(movieImageDetail);

        movieImageDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailFilm.this, MoviePoster.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("movie_poster", movie);
                intent.putExtra("movie", bundle);
                startActivity(intent);
            }
        });

        movieName = (TextView) findViewById(R.id.movieNameDetail);
        movieName.setText(movie.getJudul());
        movieDescription = (TextView) findViewById(R.id.movieDescriptionDetail);
        movieDescription.setText(movie.getDeskripsi());
        movieDate = (TextView) findViewById(R.id.movieDateDetail);
        DateFormat dateFormat =  new SimpleDateFormat("yyyy-MM-dd");
        movieDate.setText(movie.getRilis());
        try {
            Date dFormat = dateFormat.parse(movie.getRilis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.US);
            movieDate.setText(simpleDateFormat.format(dFormat));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}