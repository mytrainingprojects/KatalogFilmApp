package com.erwinsm.katalogfilm;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ListView lv;
    MovieListAdapter adapter;
    Button btnCariFilm;
    EditText movieSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = (ListView) findViewById(R.id.listView);
        adapter = new MovieListAdapter(getApplicationContext(), R.layout.movie_list_layout, new ArrayList<Movie>(), this);
        lv.setAdapter(adapter);
        lv = (ListView)findViewById(R.id.listView);

        btnCariFilm = (Button) findViewById(R.id.btn_movie_search);
        btnCariFilm.setOnClickListener(this);

        movieSearch = (EditText) findViewById(R.id.movieSearch);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailFilm.class);
                startActivity(intent);
            }
        });

        String url = "https://api.themoviedb.org/3/movie/popular?api_key=f1eae806088cf80f7f330967892d24f8";
        new ReadJSON().execute(url);

        cariFilm("");
    }


        public void cariFilm(final String query) {
        runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String space = query;
                        space = space.replace(" ", "+");
                        String url = "https://api.themoviedb.org/3/search/movie?api_key=f1eae806088cf80f7f330967892d24f8&query="
                                     +space;
                        new ReadJSON().execute(url);
                }
            });
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btn_movie_search) {
                if(movieSearch.length() != 0) {
                    this.cariFilm(movieSearch.getText().toString());
                }
            } else {
                this.cariFilm("");
            }
        }


    class ReadJSON extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params)
        {
            return readURL(params[0]);
        }

        @Override
        protected void onPostExecute(String content) {
            try {
                JSONObject jsonObject = new JSONObject(content);
                JSONArray jsonArray = jsonObject.getJSONArray("results");

                adapter.movies.clear();

                for(int i=0; i<jsonArray.length(); i++) {
                    JSONObject movieObject = jsonArray.getJSONObject(i);
                    adapter.movies.add(new Movie(
                            movieObject.getString("poster_path"),
                            movieObject.getString("title"),
                            movieObject.getString("overview"),
                            movieObject.getString("release_date")
                    ));
                }

                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private static String readURL(String theUrl) {
        StringBuilder content = new StringBuilder();
        try {
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}