package com.erwinsm.katalogfilm;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class MoviePoster extends Activity {

    Movie movie;
    Context context;
    private ImageView moviePoster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_movie_poster);

        Bundle bundle = getIntent().getBundleExtra("movie");
        movie = bundle.getParcelable("movie_poster");
        moviePoster = (ImageView)findViewById(R.id.moviePoster);
        String url =  new StringBuilder(movie.getGambar()).deleteCharAt(0).toString();
        Picasso.with(context).load("http://image.tmdb.org/t/p/w342/"+url).into(moviePoster);
    }
}
