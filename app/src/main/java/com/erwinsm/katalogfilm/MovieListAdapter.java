package com.erwinsm.katalogfilm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Erwin SM on 3/25/2018.
 */

public class MovieListAdapter extends ArrayAdapter<Movie> {

    MainActivity ma;
    ArrayList<Movie> movies;
    Context context;
    int resource;

    public MovieListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Movie> movies, MainActivity ma) {
        super(context, resource, movies);
        this.movies = movies;
        this.context = context;
        this.resource = resource;
        this.ma = ma;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.movie_list_layout, null, true);
        }
        final Movie movie = getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.movieImage);
        String url =  new StringBuilder(movie.getGambar()).deleteCharAt(0).toString();
        Picasso.with(context).load("http://image.tmdb.org/t/p/w185/"+url).into(imageView);

        TextView movieName = (TextView) convertView.findViewById(R.id.movieName);
        movieName.setText(movie.getJudul());

        TextView movieDescription = (TextView) convertView.findViewById(R.id.movieDescription);
        movieDescription.setText(movie.getDeskripsi());

        TextView movieDate = (TextView) convertView.findViewById(R.id.movieDate);
        DateFormat dateFormat =  new SimpleDateFormat("yyyy-MM-dd");
        movieDate.setText(movie.getRilis());
        try {
            Date dFormat = dateFormat.parse(movie.getRilis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.US);
            movieDate.setText(simpleDateFormat.format(dFormat));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ma, DetailFilm.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("movie_detail", movie);
                intent.putExtra("movie", bundle);
                ma.startActivity(intent);
            }
        });
        return convertView;
    }
}
