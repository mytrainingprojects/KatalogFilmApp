package com.erwinsm.katalogfilm;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Erwin SM on 3/25/2018.
 */

public class Movie implements Parcelable {
    private String gambar;
    private String judul;
    private String deskripsi;
    private String rilis;

    public Movie(String gambar, String judul, String deskripsi, String rilis) {
        this.gambar = gambar;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.rilis = rilis;
    }

    protected Movie(Parcel in) {
        gambar = in.readString();
        judul = in.readString();
        deskripsi = in.readString();
        rilis = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[0];
        }
    };

    public String getGambar() {

        return gambar;
    }

    public void setGambar(String gambar) {

        this.gambar = gambar;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getRilis() {
        return rilis;
    }

    public void setRilis(String rilis) {
        this.rilis = rilis;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(gambar);
        parcel.writeString(judul);
        parcel.writeString(deskripsi);
        parcel.writeString(rilis);
    }
}
